#ifndef __FUNCTION_PROTO__
#define __FUNCTION_PROTO__ 

void send_command(uint8_t byte);
void send_data(char byte);
void delay(uint16_t delay);

#endif