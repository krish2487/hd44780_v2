#include "unity.h"
#include "hd44780.h"
#include "stdlib.h"
#include "lcd_defs.h"
#include "mock_function_prototypes.h"

lcd_returnstatus_TypeDef return_code;
lcd_config_TypeDef lcd;

extern void send_command(uint8_t byte);
extern void send_data(char byte);
extern void delay(uint16_t delay);

void setUp(void)
{

}

void tearDown(void)
{
}

void test_lcd_init_fails_if_null_is_passed_as_argument(void)
{

	return_code = lcd_init(NULL);

	TEST_ASSERT_EQUAL(LCD_ERROR, return_code);

}

void test_lcd_init_fails_if_structure_members_are_not_set(void)
{
	return_code = lcd_init(&lcd);
	TEST_ASSERT_EQUAL(LCD_ERROR, return_code);
}

void test_lcd_init_fails_when_all_parameters_are_set_but_fnptrs_are_not_pointing_to_functions(void)
{

	data_ptr lcd_send_data;
	command_ptr lcd_send_command;
	delay_ptr delay_microseconds;

	lcd_send_data = NULL;
	lcd_send_command = NULL;
	delay_microseconds = NULL;

	lcd.function_set = FUNCTION_SET_BASE;
	lcd.entry_mode = ENTRY_MODE_BASE;
	lcd.display_shift = DISPLAY_SHIFT_BASE;
	lcd.display_control = DISPLAY_CONTROL_BASE;
	lcd.delay_useconds = delay_microseconds;
	lcd.delay_mseconds = delay_microseconds;
	lcd.lcd_send_databyte = lcd_send_data;
	lcd.lcd_send_commandbyte = lcd_send_command;

	return_code = lcd_init(&lcd);
	TEST_ASSERT_EQUAL(LCD_ERROR, return_code);
}

void test_lcd_init_all_mock_functions_are_called_in_order(void)
{


	lcd.function_set = FUNCTION_SET_BASE;
	lcd.entry_mode = ENTRY_MODE_BASE;
	lcd.display_shift = DISPLAY_SHIFT_BASE;
	lcd.display_control = DISPLAY_CONTROL_BASE;
	lcd.delay_useconds = delay;
	lcd.delay_mseconds = delay;
	lcd.lcd_send_databyte = send_data;
	lcd.lcd_send_commandbyte = send_command;

	send_command_Expect(lcd.function_set);
	delay_Expect(5);
	send_command_Expect(lcd.function_set);
	delay_Expect(100);
	send_command_Expect(lcd.function_set);
	delay_Expect(100);
	send_command_Expect(lcd.function_set);
	delay_Expect(100);
	send_command_Expect(DISPLAY_CONTROL_BASE | DISPLAY_CONTROL_OFF);
	delay_Expect(100);
	send_command_Expect(lcd.entry_mode);
	delay_Expect(100);
	send_command_Expect(lcd.display_shift);
	delay_Expect(100);
	send_command_Expect(lcd.display_control);
	delay_Expect(100);
	send_command_Expect(RETURN_HOME);
	delay_Expect(100);
	send_command_Expect(CLEAR_DISPLAY);
	delay_Expect(100);

	return_code = lcd_init(&lcd);
	TEST_ASSERT_EQUAL(LCD_OK, return_code);
}

void test_lcd_clear_display_returns_error_if_fnptr_is_null(void)
{

	lcd.lcd_send_commandbyte = NULL;
	send_command_Ignore();
	return_code = lcd_clear_display(&lcd);
	TEST_ASSERT_EQUAL(LCD_ERROR, return_code);
}

void test_lcd_clear_display_returns_ok(void)
{
	lcd.lcd_send_commandbyte = send_command;
	send_command_Ignore();
	return_code = lcd_clear_display(&lcd);
	TEST_ASSERT_EQUAL(LCD_OK, return_code);
}

void test_lcd_clear_display_calls_send_command_with_correct_parameter(void)
{

	lcd.lcd_send_commandbyte = send_command;
	send_command_Expect(CLEAR_DISPLAY);
	lcd_clear_display(&lcd);
}

void test_lcd_goto_home_returns_error_if_fnptr_is_null(void)
{

	lcd.lcd_send_commandbyte = NULL;
	send_command_Ignore();
	return_code = lcd_goto_home(&lcd);
	TEST_ASSERT_EQUAL(LCD_ERROR, return_code);
}

void test_lcd_goto_home_returns_ok(void)
{
	lcd.lcd_send_commandbyte = send_command;
	send_command_Ignore();
	return_code = lcd_goto_home(&lcd);
	TEST_ASSERT_EQUAL(LCD_OK, return_code);

}

void test_lcd_goto_home_calls_send_command_with_correct_parameter(void)
{
	lcd.lcd_send_commandbyte = send_command;
	send_command_Expect(RETURN_HOME);
	lcd_goto_home(&lcd);

}

void test_lcd_send_data_returns_ok(void)
{
	send_data_Ignore();
	return_code = lcd_send_data("Hello World",11,&lcd);
	TEST_ASSERT_EQUAL(LCD_OK, return_code);

}

void test_lcd_send_data_for_negative_length(void)
{
	return_code = lcd_send_data("Hello World", -3, &lcd);
	TEST_ASSERT_EQUAL(LCD_ERROR, return_code);
}


void test_lcd_send_data_returns_error_when_length_and_data_are_not_equal(void)
{
	return_code = lcd_send_data("Hello World", 20, &lcd);
	TEST_ASSERT_EQUAL(LCD_ERROR, return_code);
}


void test_lcd_send_data_returns_error_when_data_is_less_than_length(void)
{
	return_code = lcd_send_data("Hello", 20, &lcd);
	TEST_ASSERT_EQUAL(LCD_ERROR, return_code);
}

void test_lcd_send_data_returns_error_when_data_is_greater_than_length(void)
{
	return_code = lcd_send_data("Hello", 3, &lcd);
	TEST_ASSERT_EQUAL(LCD_ERROR, return_code);
}

void test_lcd_send_data_returns_error_when_data_has_null_terminator_in_middle(void)
{
	return_code = lcd_send_data("Hello\0 World", 12, &lcd);
	TEST_ASSERT_EQUAL(LCD_ERROR, return_code);
}

void test_lcd_send_data_calls_send_data_correctly(void)
{
	send_data_Expect('H');
	send_data_Expect('E');
	send_data_Expect('L');
	send_data_Expect('L');
	send_data_Expect('O');
	return_code = lcd_send_data("HELLO", 5, &lcd);
	TEST_ASSERT_EQUAL(LCD_OK, return_code);
}

void test_lcd_goto_returns_ok(void)
{
	send_command_Ignore();
	return_code = lcd_goto(2, 4, &lcd);
	TEST_ASSERT_EQUAL(LCD_OK, return_code);
}

void test_lcd_goto_returns_error_on_negative_input(void)
{

	return_code = lcd_goto(-2, -4, &lcd);
	TEST_ASSERT_EQUAL(LCD_ERROR, return_code);
}

void test_lcd_goto_returns_error_on_greater_than_4_rows(void)
{

	return_code = lcd_goto(6, 1, &lcd);
	TEST_ASSERT_EQUAL(LCD_ERROR, return_code);
}

void test_lcd_goto_calls_lcd_command_with_correct_arguments(void)
{
	send_command_Expect(0x96);
	return_code = lcd_goto(2, 2, &lcd);
	TEST_ASSERT_EQUAL(LCD_OK, return_code);
}
