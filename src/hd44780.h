#ifndef _HD44780_H
#define _HD44780_H
#include "lcd_defs.h"

/**
 * @brief      { Init function to set up the lcd as per the config structure passed as parameter. }
 *
 * @param      Pointer to structure of type lcd_config_TypeDef
 *
 * @return     { returns LCD_OK if the input parameter is set up correctly, otherwise returns LCD_ERROR }
 */
lcd_returnstatus_TypeDef lcd_init(lcd_config_TypeDef *device_config);
/**
 * @brief      { Clears the lcd display }
 *
 * @param      Pointer to structure of type lcd_config_TypeDef
 *
 * @return     { returns LCD_OK if the input parameter is set up correctly, otherwise returns LCD_ERROR }
 */
lcd_returnstatus_TypeDef lcd_clear_display(lcd_config_TypeDef *device_config);
/**
 * @brief      { Moves the cursor to 1st row of 1st column }
 *
 * @param      Pointer to structure of type lcd_config_TypeDef
 *
 * @return     { returns LCD_OK if the input parameter is set up correctly, otherwise returns LCD_ERROR }
 */
lcd_returnstatus_TypeDef lcd_goto_home(lcd_config_TypeDef *device_config);
/**
 * @brief      { prints a string on the lcd }
 *
 * @param      data               string to be printed
 * @param[in]  length             length of the string
 * @param      lcd_device_struct  Pointer to structure of type lcd_config_TypeDef
 *
 * @return     { returns LCD_OK if the input parameter is set up correctly, otherwise returns LCD_ERROR }
 */
lcd_returnstatus_TypeDef lcd_send_data(char *data, int8_t length,lcd_config_TypeDef  *lcd_device_struct);
/**
 * @brief      { Moves the cursor specified by the row, column. Indexed from 0. }
 *
 * @param[in]  row                The row
 * @param[in]  column             The column
 * @param      lcd_device_struct  Pointer to structure of type lcd_config_TypeDef
 *
 * @return     { returns LCD_OK if the input parameter is set up correctly, otherwise returns LCD_ERROR }
 */
lcd_returnstatus_TypeDef lcd_goto(int8_t row, int8_t column,lcd_config_TypeDef  *lcd_device_struct);


#endif // _HD44870_H
