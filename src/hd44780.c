#include "hd44780.h"
#include "assert.h"

lcd_returnstatus_TypeDef lcd_init(lcd_config_TypeDef *device_config)
{

    if (device_config == NULL || device_config->delay_mseconds == NULL || device_config->delay_useconds == NULL \
        || device_config->lcd_send_databyte == NULL || device_config->lcd_send_commandbyte == NULL)
        {
            return LCD_ERROR;
        }

    device_config->lcd_send_commandbyte(device_config->function_set);
    device_config->delay_mseconds(5);
    device_config->lcd_send_commandbyte(device_config->function_set);
    device_config->delay_useconds(100);
    device_config->lcd_send_commandbyte(device_config->function_set);
    device_config->delay_useconds(100);
    device_config->lcd_send_commandbyte(device_config->function_set);
    device_config->delay_useconds(100);
    device_config->lcd_send_commandbyte(DISPLAY_CONTROL_BASE | DISPLAY_CONTROL_OFF);
    device_config->delay_mseconds(100);
    device_config->lcd_send_commandbyte(device_config->entry_mode);
    device_config->delay_useconds(100);
    device_config->lcd_send_commandbyte(device_config->display_shift);
    device_config->delay_useconds(100);
    device_config->lcd_send_commandbyte(device_config->display_control);
    device_config->delay_mseconds(100);
    device_config->lcd_send_commandbyte(RETURN_HOME);
    device_config->delay_mseconds(100);
    device_config->lcd_send_commandbyte(CLEAR_DISPLAY);
    device_config->delay_mseconds(100);


    return LCD_OK;
}

lcd_returnstatus_TypeDef lcd_clear_display(lcd_config_TypeDef *device_config)
{
	if(device_config->lcd_send_commandbyte == NULL)
	{
		return LCD_ERROR;
	}
    device_config->lcd_send_commandbyte(CLEAR_DISPLAY);
    return LCD_OK;
}

lcd_returnstatus_TypeDef lcd_goto_home(lcd_config_TypeDef *device_config)
{
	if(device_config->lcd_send_commandbyte == NULL)
	{
		return LCD_ERROR;
	}
    device_config->lcd_send_commandbyte(RETURN_HOME);
    return LCD_OK;
}

lcd_returnstatus_TypeDef lcd_send_data(char *data, int8_t length,lcd_config_TypeDef  *device_config)
{
	//Checks if negative length is provided, string is not
	//null terminated or string length is greater than 
	//the length provided
    if(length < 0 || data[length] != '\0' )
    {
        return LCD_ERROR;
    }

    // checks if there is any null terminator in the middle of the string provided
    for(uint8_t i =0; i < length;i++)
    {
    	if(data[i] == '\0')
    	{
    		return LCD_ERROR;
    	}
    }

    while(length != 0 )
    {
        device_config->lcd_send_databyte(*data);
        length--;
        data++;
    }

    return LCD_OK;
}

lcd_returnstatus_TypeDef lcd_goto(int8_t row, int8_t column,lcd_config_TypeDef *device_config)
{

    if(row <0 || column <0 || row > 4 || column > 20)
    {
        return LCD_ERROR;
    }

    int row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };

    device_config->lcd_send_commandbyte(SET_CURSOR_BASE | (row + row_offsets[column]));

    return LCD_OK;
}
